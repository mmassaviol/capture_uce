tabquality_control = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectquality_control", label = "", value="fastqc")),box(title = "FastQC", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		p("FastQC: A quality control tool for high throughput raw sequence data."),

		p("Website : ",a(href="https://www.bioinformatics.babraham.ac.uk/projects/fastqc/","https://www.bioinformatics.babraham.ac.uk/projects/fastqc/",target="_blank")),

		p("Documentation : ",a(href="http://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/","http://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/",target="_blank"))

	)))


