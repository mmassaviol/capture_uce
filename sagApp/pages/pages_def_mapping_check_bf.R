tabmapping_check_bf = fluidPage(

box(title = "Parameters :", width = 12, status = "primary", collapsible = TRUE, solidHeader = TRUE,

	hidden(textInput("selectmapping_check_bf", label = "", value="samtools_stats")),box(title = "SAM tools stats", width = 12, status = "success", collapsible = TRUE, solidHeader = TRUE,
		numericInput("mapping_check_bf__samtools_stats_threads", label = "Number of threads to use", min = 1, max = NA, step = 1, width =  "auto", value = 4),

		p("SAM tools stats: The program collects statistics from BAM files"),

		p("Website : ",a(href="http://samtools.sourceforge.net/","http://samtools.sourceforge.net/",target="_blank")),

		p("Documentation : ",a(href="http://www.htslib.org/doc/samtools.html","http://www.htslib.org/doc/samtools.html",target="_blank")),

		p("Paper : ",a(href="https://doi.org/10.1093/bioinformatics/btp352","https://doi.org/10.1093/bioinformatics/btp352",target="_blank"))

	)))


