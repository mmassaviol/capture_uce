FROM mbbteam/mbb_workflows_base:latest as alltools

RUN apt -y update && apt install -y fastqc=0.11.5+dfsg-6

RUN cd /opt/biotools \
 && wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip \
 && unzip Trimmomatic-0.38.zip \
 && echo -e '#!/bin/bash java -jar /opt/biotools/Trimmomatic-0.38/trimmomatic-0.38.jar' > bin/trimmomatic \
 && chmod 777 bin/trimmomatic \
 && rm Trimmomatic-0.38.zip

RUN apt -y update && apt install -y openjdk-8-jre

RUN cd /opt/biotools \
 && wget -O FLASH-1.2.11.tar.gz https://sourceforge.net/projects/flashpage/files/FLASH-1.2.11.tar.gz \
 && tar -xvzf FLASH-1.2.11.tar.gz \
 && cd FLASH-1.2.11 \
 && make \
 && mv flash ../bin \
 && cd .. \
 && rm -r FLASH-1.2.11 FLASH-1.2.11.tar.gz

RUN apt-get install -y parallel

RUN cd /opt/biotools \
 && wget -O velvet-1.2.10.tar.gz https://github.com/dzerbino/velvet/archive/v1.2.10.tar.gz \
 && tar -xvzf velvet-1.2.10.tar.gz \
 && cd velvet-1.2.10 \
 && make -j 10 \
 && mv velvet* ../bin \
 && cd .. \
 && rm -r velvet-1.2.10 velvet-1.2.10.tar.gz

RUN cd /opt/biotools \
 && wget -O lastz-1.04.03.tar.gz http://www.bx.psu.edu/~rsharris/lastz/lastz-1.04.03.tar.gz \
 && tar -xvzf lastz-1.04.03.tar.gz \
 && cd lastz-distrib-1.04.03/src \
 && make -j 10 \
 && mv lastz* /opt/biotools/bin/ \
 && cd ../.. \
 && rm -r lastz-distrib-1.04.03 lastz-1.04.03.tar.gz

RUN cd /opt/biotools \
 && wget https://github.com/samtools/samtools/releases/download/1.9/samtools-1.9.tar.bz2 \
 && tar -xvjf samtools-1.9.tar.bz2 \
 && cd samtools-1.9 \
 && ./configure && make \
 && cd .. \
 && mv samtools-1.9/samtools bin/samtools \
 && rm -r samtools-1.9 samtools-1.9.tar.bz2

RUN cd /opt/biotools \
 && wget ftp://emboss.open-bio.org/pub/EMBOSS/EMBOSS-6.6.0.tar.gz \
 && tar -xvzf EMBOSS-6.6.0.tar.gz \
 && cd EMBOSS-6.6.0 \
 && ./configure --without-x \
 && make -j 8 \
 && cd ../bin && ln -s /opt/biotools/EMBOSS-6.6.0/emboss/seqret seqret \
 && rm ../EMBOSS-6.6.0.tar.gz

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

