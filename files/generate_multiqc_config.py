import re
import sys
from tools import read_yaml

config = read_yaml(sys.argv[1])

def files_or_dirs_to_ignore():
    # files
    res = "fn_ignore_files:\n"
    file_ignore = ["*.fa","*.fasta","*.fa.gz","*.fasta.gz","*.fq","*.fastq","*.fq.gz","*.fastq.gz","*.sam","*.bam","*.gtf","*.gtf.gz","*.vcf","*.vcf.gz"]
    for file in file_ignore:
        res += "  - '" + file + "'\n"

    res += "\n"
    res += "fn_ignore_dirs:\n"
    dirs_ignore = ["workflow"]
    for dir in dirs_ignore:
        res += "  - '" + dir + "'\n"

    res += "\n"

    return res

def module_order():
    res = "module_order:\n"
    for step in config["steps"]:
        tool = config["params"][step["name"]]
        if (config["multiqc"][tool] != "custom"):
            res += "  - " + config["multiqc"][tool] + ":\n"
            res += "      name: " + step["title"] + " (" + tool + ")\n"
            res += "      anchor: " + step["name"] + "__" + config["multiqc"][tool] + "\n"
            res += "      path_filters:\n"
            for rule in config["outputs"][step["name"] + "__" + tool].keys():
                res += "        - '*" + config["params"][rule + "_output_dir"] + "/*'\n" # limit search to tool output dir
                res += "        - '*/logs/" + config["params"][rule + "_output_dir"] + "/*'\n" # and tool logs
    return res

def report_section_order():
    res = "report_section_order:\n"
    res += "  Rule_graph:\n" 
    res += "    order: 990\n"
    res += "  params_tab:\n" 
    res += "    order: 980\n"
    res += "  outputs:\n" 
    res += "    order: 970\n"
    res += "  Tools_version:\n" 
    res += "    order: 960\n"
    res += "  Citations:\n" 
    res += "    order: -1000\n"
    cpt = 950
    for step in config["steps"]:
        tool = config["params"][step["name"]]
        if (config["multiqc"][tool] != "custom"):
            res += "  " + step["name"] + "__" + config["multiqc"][tool] + ":\n"
            res += "    " + "order: " + str(cpt) + "\n"
            cpt += -10
        for rule in config["outputs"][step["name"] + "__" + tool]:
            if ("SeOrPe" not in config.keys() or (config["params"]["SeOrPe"] == "SE" and not("_PE" in rule)) or (config["params"]["SeOrPe"] == "PE" and not("_SE" in rule))):
                for output in config["outputs"][step["name"] + "__" + tool][rule]:
                    if("file" in output.keys() and "mqc" in output["file"] and '{' not in output["file"]): # case of dynamic files ({wildcard}_mqc.png) to deal with
                        section = re.sub('\_mqc.*$', '', output["file"])
                        res += "  " + section + ":\n" 
                        res += "    " + "order: " + str(cpt) + "\n"
                        cpt += -10
                if step["name"] + "__" + tool in config["prepare_report_outputs"]:
                    if isinstance(config["prepare_report_outputs"][step["name"] + "__" + tool], list):
                        for output in config["prepare_report_outputs"][step["name"] + "__" + tool]:
                            section = re.sub('\_mqc.*$', '', output)
                            res += "  " + section + ":\n" 
                            res += "    " + "order: " + str(cpt) + "\n"
                            cpt += -10
                    else:
                        section = re.sub('\_mqc.*$', '', config["prepare_report_outputs"][step["name"] + "__" + tool])
                        res += "  " + step["name"] + "__" + section + ":\n" 
                        res += "    " + "order: " + str(cpt) + "\n"
                        cpt += -10
    return res

def main():
    res = "skip_generalstats: true\n\n"
    res += module_order() + "\n\n"
    res += report_section_order() + "\n\n"
    res += files_or_dirs_to_ignore()

    with open(sys.argv[2],"w") as out:
        out.write(res)

if __name__ == "__main__":
    # execute only if run as a script
    main()